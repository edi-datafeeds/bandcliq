EDI_CPOPN_20170201_1
TableName	Actflag	Created	Changed	CpopnID	SecID	CallPut	Notes
CPOPN	U	2015/08/27	2017/02/01	157935	4593340	C	
CPOPN	U	2016/03/14	2017/02/01	175725	4726006	C	
CPOPN	U	2017/01/27	2017/02/01	210097	5031541	C	
CPOPN	U	2017/01/30	2017/02/01	210261	4576145	C	We may redeem some or all of the Notes at any time prior to August 11, 2019 at a price equal to 100% of the<BR>principal amount of the Notes plus accrued and unpaid interest, if any, to the date of redemption plus a  makewhole <BR>premium set forth in this document.<BR><BR>Applicable Premium  means, as determined by the Issuer with respect to a Note on any<BR>date of redemption, the greater of:<BR>(1) 1.0% of the principal amount of such Note; and<BR>(2) the excess, if any, of (a) the present value as of such date of redemption of (i) the<BR>redemption price of such Note on August 11, 2019 (such redemption price being described in<BR>the table appearing under   Optional redemption ), plus (ii) the remaining scheduled interest<BR>payments due on such Note through August 11, 2019 (excluding accrued but unpaid interest to<BR>the date of redemption), computed using a discount rate equal to the Treasury Rate as of such<BR>date of redemption plus 50 basis points, over (b) the then outstanding principal amount of such<BR>Note.<BR><BR>
CPOPN	U	2017/01/31	2017/02/01	210513	5032663	C	
CPOPN	U	2017/01/31	2017/02/01	210687	5034088	C	
CPOPN	U	2017/01/31	2017/02/01	210772	5034529	C	Optional Redemption:<BR>The Corporation shall have the option to redeem the Series A Preferred Shares, in whole or in part, on or after March 31, 2022, for cash at a redemption price of USD25.00 per Series A Preferred Share, plus accumulated and unpaid dividends. <BR><BR>Maturity Date:<BR>Perpetual (unless redeemed by the Corporation on or after March 31, 2022).
CPOPN	I	2017/02/01	2017/02/01	210790	5031832	C	
CPOPN	I	2017/02/01	2017/02/01	210791	4899775	C	Early  Redemption at the Option of the  Issuer: We  may redeem your Notes  (in whole but not in part)  at our sole discretion without your  consent at the Redemption Price set forth below on any Contingent Coupon  Payment Date <BR>Contingent Coupon Payment Dates: With respect to any Observation Date, the fifth Business Day after such Observation Date, provided that the Contingent Coupon Payment Date with respect to the Final Valuation Date will be the Maturity Date <BR>Observation Dates: The 12 of each March, June, September and December during the  term of the Notes, beginning in December 2016,  providedthat the final Observation Date will be the Final Valuation Date
CPOPN	I	2017/02/01	2017/02/01	210795	5034749	C	
CPOPN	I	2017/02/01	2017/02/01	210797	5034136	C	
CPOPN	I	2017/02/01	2017/02/01	210798	5034081	C	
CPOPN	I	2017/02/01	2017/02/01	210799	5034084	C	
CPOPN	I	2017/02/01	2017/02/01	210802	5034936	C	
CPOPN	I	2017/02/01	2017/02/01	210806	5035052	C	
CPOPN	I	2017/02/01	2017/02/01	210808	5035089	C	
CPOPN	I	2017/02/01	2017/02/01	210812	5032641	C	
CPOPN	I	2017/02/01	2017/02/01	210813	5035119	C	
CPOPN	I	2017/02/01	2017/02/01	210816	5032617	C	
CPOPN	I	2017/02/01	2017/02/01	210817	5035129	C	
CPOPN	I	2017/02/01	2017/02/01	210820	5035141	C	
CPOPN	I	2017/02/01	2017/02/01	210823	5032040	C	
CPOPN	I	2017/02/01	2017/02/01	210825	5032644	C	
CPOPN	I	2017/02/01	2017/02/01	210830	5035205	C	
CPOPN	I	2017/02/01	2017/02/01	210832	5035218	C	
CPOPN	I	2017/02/01	2017/02/01	210833	5032545	C	
CPOPN	I	2017/02/01	2017/02/01	210835	5035230	C	
CPOPN	I	2017/02/01	2017/02/01	210836	5032623	C	
CPOPN	I	2017/02/01	2017/02/01	210837	5032860	C	
CPOPN	I	2017/02/01	2017/02/01	210838	5032673	C	
CPOPN	I	2017/02/01	2017/02/01	210840	5032635	C	
CPOPN	I	2017/02/01	2017/02/01	210843	5035308	C	
CPOPN	U	2013/11/29	2017/02/01	72861	990962	C	
CPOPN	U	2013/11/29	2017/02/01	72862	990962	P	
CPOPN	U	2016/09/27	2017/02/01	194160	4915630	C	
CPOPN	U	2016/09/27	2017/02/01	194162	4915638	C	
CPOPN	U	2016/12/30	2017/02/01	205389	5006314	C	Applicable Premium  means, with respect to any Note being redeemed on any Redemption Date, the greater of:<BR>(1) 1.00% of the principal amount of such Note; and<BR>(2) the excess, if any, of (a) the present value at such Redemption Date of (i) the redemption price of such Note at February 1, 2019 (such redemption<BR>price being set forth in the table appearing above under the caption   Optional Redemption ), plus (ii) all required remaining scheduled interest payments<BR>due on such Note through February 1, 2019 (excluding accrued but unpaid interest to the Redemption Date), computed using a discount rate equal to the<BR>Treasury Rate as of such Redemption Date plus 50 basis points; over (b) the then outstanding principal amount of such Note.<BR>The Issuer shall calculate or cause to be calculated the Applicable Premium and the Trustee shall have no duty to calculate or verify the Issuer s calculation<BR>of the Applicable Premium.
CPOPN	U	2017/01/24	2017/02/01	209495	5027878	C	
CPOPN	U	2017/01/30	2017/02/01	210397	5030675	C	
CPOPN	U	2017/01/31	2017/02/01	210619	5034126	C	
CPOPN	U	2017/01/31	2017/02/01	210674	5034182	C	
CPOPN	U	2017/01/31	2017/02/01	210767	5034190	C	At any time prior to February 6, 2020 at T+10 basis points
CPOPN	U	2017/01/31	2017/02/01	210768	5034176	C	
CPOPN	U	2017/01/31	2017/02/01	210769	5034179	C	
CPOPN	U	2017/01/31	2017/02/01	210771	5034186	C	At any time prior to January 6, 2022 at T+10 basis points
CPOPN	U	2017/01/31	2017/02/01	210775	5034149	C	100% of the principal amount of the notes of such series to be redeemed and 20 basis points
CPOPN	U	2017/01/31	2017/02/01	210776	5034141	C	
CPOPN	U	2017/02/01	2017/02/01	210809	5035101	C	Optional Redemption:<BR>The issuer may redeem the bonds at any time, prior to December 7, 2023, in whole or in part, at a `make-whole`<BR>redemption price equal to the greater of (1) 100% of the principal amount being redeemed or (2) the sum of the present<BR>values of the remaining scheduled payments of principal and interest (other than accrued interest) on the bonds being<BR>redeemed that would be due if such bonds matured on December 7, 2023, discounted to the redemption date on a semiannual<BR>basis (assuming a 360-day year consisting of twelve 30-day months) at the Treasury Rate, plus 15 basis points<BR>for the bonds plus in each case of (1) and (2) above, accrued interest to, but excluding, the redemption date.<BR><BR>At any time on or after December 7, 2023, the issuer may redeem the bonds at its option, in whole or in part, at a<BR>redemption price equal to 100% of the principal amount of the bonds then outstanding to be redeemed, plus accrued and<BR>unpaid interest on the bonds being redeemed to, but excluding, the redemption date.
CPOPN	U	2017/02/01	2017/02/01	210815	5035127	C	OPTIONAL REDEMPTION: Each series of Notes may be redeemed at any time prior to the applicable Par Call Date (as set forth<BR>in the table below), in whole or from time to time in part, at a make-whole call equal to the greater of (i) 100% of the principal amount of the Notes of such series to be redeemed or (ii) the sum of the present values of the remaining scheduled payments of principal and interest discounted to the redemption date, on a semiannual basis (assuming a 360-day year consisting of twelve 30-day<BR>months), at a rate equal to the sum of the Treasury Rate plus a number of basis points equal to the applicable Make-Whole Spread (as set forth in the table below). <BR><BR>Each series of Notes may be redeemed at any time on or after the applicable Par Call Date, in whole or in part, at a redemption<BR>price equal to 100% of the principal amount of such series of Notes to be redeemed. Accrued interest will be payable to the redemption date. <BR>Make-Whole Spread: 40 bps<BR>
CPOPN	U	2017/02/01	2017/02/01	210824	5035175	C	OPTIONAL REDEMPTION: Each series of Notes may be redeemed at any time prior to the applicable Par Call Date (as set forth<BR>in the table below), in whole or from time to time in part, at a make-whole call equal to the greater of (i) 100% of the principal amount of the Notes of such series to be redeemed or (ii) the sum of the present values of the remaining scheduled payments of principal and interest discounted to the redemption date, on a semiannual basis (assuming a 360-day year consisting of twelve 30-day<BR>months), at a rate equal to the sum of the Treasury Rate plus a number of basis points equal to the applicable Make-Whole Spread (as set forth in the table below). <BR><BR>Each series of Notes may be redeemed at any time on or after the applicable Par Call Date, in whole or in part, at a redemption<BR>price equal to 100% of the principal amount of such series of Notes to be redeemed. Accrued interest will be payable to the redemption date. <BR>Make-Whole Spread: 40 bps<BR>
CPOPN	U	2017/02/01	2017/02/01	210828	5035193	C	
CPOPN	U	2017/02/01	2017/02/01	210829	5035203	C	
CPOPN	U	2017/02/01	2017/02/01	210831	5035217	C	
CPOPN	U	2017/02/01	2017/02/01	210834	5035227	C	
CPOPN	I	2017/02/01	2017/02/01	210863	5035274	C	
CPOPN	I	2017/02/01	2017/02/01	210871	5035514	C	
CPOPN	I	2017/02/01	2017/02/01	210945	5035270	C	
EDI_ENDOFFILE
